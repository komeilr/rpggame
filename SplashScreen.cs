﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpggame
{
    class SplashScreen: GameScreen
    {

        private Texture2D _texture;
        string path;

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, Vector2.Zero, Color.White);
        }

        public override void LoadContent()
        {
            base.LoadContent();
            path = "images/SplashScreenBG";
            _texture = _content.Load<Texture2D>(path);
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
