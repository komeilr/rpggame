﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace rpggame
{
    class ScreenManager
    {

        private static ScreenManager _instance;
        public Vector2 Dimensions { private set; get;}
        public ContentManager Content { private set; get; }
        public GameScreen currentScreen;

        /// <summary>
        /// Singleton that manages active GameScreen
        /// </summary>
        public static ScreenManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ScreenManager();
                return _instance;
            }   
        }

        public ScreenManager()
        {
            Dimensions = new Vector2(1280, 786);
            currentScreen = new SplashScreen();
        }
        public void LoadContent(ContentManager content)
        {
            this.Content = new ContentManager(content.ServiceProvider, "Content");
            currentScreen.LoadContent();
        }

        public void UnloadContent()
        {
            currentScreen.UnloadContent();
        }

        public void Update(GameTime gameTime)
        {
            currentScreen.Update(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            currentScreen.Draw(spriteBatch);
        }
    }
}
