﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace rpggame
{
    public class GameScreen
    {
        protected ContentManager _content;

        public virtual void LoadContent() 
        {
            _content = new ContentManager(
                ScreenManager.Instance.Content.ServiceProvider, "Content");
        }

        public virtual void UnloadContent() 
        {
            _content.Unload();
        }

        public virtual void Update(GameTime gameTime) 
        {
        }

        public virtual void Draw(SpriteBatch spriteBatch) 
        {
            
        }
    }
}
